package deployment

import s ".../service:service"

#Deployment: {
  name: "volumesdep"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {
      store: volume: "myvolume"
    }
    scale: detail: frontend: hsize: 1
    resilience: 0
  }
}
