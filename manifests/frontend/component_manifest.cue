package component

import k "kumori.systems/kumori:kumori"

#Artifact: {
  ref: name:  "frontend"

  description: {

    srv: {
      server: {
        restapi: { protocol: "http", port: 8080 }
      }
    }

    config: {
      parameter: {}
      resource: {
        store: k.#Volume
      }
    }

    size: {
      bandwidth: { size: 10, unit: "M" }
    }

    probe: frontend: {
      liveness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        startupGraceWindow: { unit: "ms", duration: 180000, probe: true }
        frequency: "medium"
        timeout: 30000  // msec
      }
      readiness: {
        protocol: http : { port: srv.server.restapi.port, path: "/health" }
        frequency: "medium"
        timeout: 30000 // msec
      }
    }

    code: {
      frontend: {
        name: "frontend"
        image: {
          tag: "kumoripublic/examples-volumes-frontend:v2.0.0"
        }
        mapping: {
          filesystem: {
            "/config/config.json": {
              data: value: {
                path: "/data/store"
              },
              format: "json"
            },
            "/data/store": {
              volume: "store"
            }
          }
          env: {
            CONFIG_FILE: value: "/config/config.json"
            HTTP_SERVER_PORT_ENV: value: "\(srv.server.restapi.port)"
          }
        }
        size: {
          memory: { size: 200, unit: "M" }
          mincpu: 200
          cpu: { size: 250, unit: "m" }
        }
      }
    }
  }
}
