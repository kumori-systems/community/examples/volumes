package service

import (
  k "kumori.systems/kumori:kumori"
  f ".../frontend:component"
)

#Artifact: {
  ref: name:  "service"

  description: {

    config: {
      parameter: {}
      resource: {
        store: k.#Volume
      }
    }

    role: {
      frontend: {
        artifact: f.#Artifact
        config: {
          parameter: {}
          resource: {
            store: description.config.resource.store
          }
          resilience: description.config.resilience
        }
      }
    }

    srv: {
      server: {
        restapi: { protocol: "http", port: 80 }
      }
    }

    connect: {
      cinbound: {
        as: "lb"
	  		from: self: "restapi"
        to: frontend: "restapi": _
      }
    }
  }
}
