#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
INBOUNDNAME="volumesinb"
DEPLOYNAME="volumesdep"
DOMAIN="volumesdomain"
VOLUME="myvolume"
SERVICEURL="volumes-${CLUSTERNAME}.${REFERENCEDOMAIN}"
VOLUMETYPE="${VOLUMETYPE:-cinder-regular}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"

# Change if special binaries want to be used
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-volume')
  ${KAM_CTL_CMD} register volume $VOLUME \
    --items 5 \
    --size 100Mi \
    --type $VOLUMETYPE
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

'deploy-inbound')
  ${KAM_CTL_CMD} register inbound $INBOUNDNAME \
    --domain $DOMAIN \
    --cert $CLUSTERCERT
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;

'deploy-service-persistent')
  ${KAM_CMD} service deploy -d deployments/persistent -t ./manifests $DEPLOYNAME -- \
    --comment "Volumes example service" \
    --wait 5m
  ;;

'deploy-service-volatile')
  ${KAM_CMD} service deploy -d deployments/volatile -t ./manifests $DEPLOYNAME -- \
    --comment "Volumes example service" \
    --wait 5m
  ;;

'link')
  ${KAM_CTL_CMD} link $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'deploy-persistent')
  $0 create-volume
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service-persistent
  $0 link
  ;;

'deploy-volatile')
  $0 create-domain
  $0 deploy-inbound
  $0 deploy-service-volatile
  $0 link
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  echo
  ${KAM_CTL_CMD} describe volume $VOLUME
  ;;

# Test the volumes service
'test')
  echo -----------------------------------------
  echo Getting not existing mykey1; echo
  echo get: ; curl -X GET  https://$SERVICEURL/keys//mykey1; echo
  echo -----------------------------------------
  echo Storing mykey1; echo
  echo post: ; curl -X POST  https://$SERVICEURL/keys/mykey1/myvalue1; echo
  echo -----------------------------------------
  echo Getting existing mykey1; echo
  echo get: ; curl -X GET  https://$SERVICEURL/keys/mykey1; echo
  echo -----------------------------------------
  echo Deleting existing mykey1; echo
  echo delete: ; curl -X DELETE  https://$SERVICEURL/keys/mykey1; echo
  echo -----------------------------------------
  echo Getting not existing mykey1; echo
  echo get: ; curl -X GET  https://$SERVICEURL/keys/mykey1; echo
  echo -----------------------------------------
  ;;

'unlink')
  ${KAM_CTL_CMD} unlink $DEPLOYNAME:restapi $INBOUNDNAME:inbound
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --force --wait 5m
  ;;

'undeploy-inbound')
  ${KAM_CMD} service undeploy $INBOUNDNAME -- --wait 5m
  ;;

'delete-volume')
  ${KAM_CTL_CMD} unregister volume $VOLUME
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

'undeploy-persistent')
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-volume
  $0 delete-domain
  ;;

'undeploy-volatile')
  $0 undeploy-service
  $0 undeploy-inbound
  $0 delete-domain
  ;;

*)
  echo "This script doesn't contain that command"
	;;

esac
